package libraryManagement;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ForgotPassword extends JFrame {
	Connection con;
	ResultSet rs;
	PreparedStatement pst;

	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ForgotPassword frame = new ForgotPassword();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws ClassNotFoundException 
	 */
	///Creating search method for userName
	public void search() {
		//String a1=textField_1.getText();
		String sql="select * from account where username=?";
	   try {
		pst=con.prepareStatement(sql);
	pst.setString(1,textField_1.getText());
		rs=pst.executeQuery();
		
		if(rs.next()) {
		
			textField_2.setText(rs.getString(3));	
			//textField.setText(rs.getString());
		}
		else {
			JOptionPane.showMessageDialog(null, "username doesn't exit");

		}
	} catch (SQLException e) {
		e.printStackTrace();
		JOptionPane.showMessageDialog(null, e);
		
	}
	}
	
	public void retrive() {
		String sql="select * from account where answer=? and username=?";
		try {
			pst=con.prepareStatement(sql);
			pst.setString(1,textField_3.getText());
			pst.setString(2,textField_1.getText());
			rs=pst.executeQuery();
		
			if(rs.next()) {
				textField_4.setText(rs.getString(2));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e);
		}

		
	}
	
	public ForgotPassword() throws ClassNotFoundException {
		super("Forgot Password");
		con= DatabaseConection.connectionDB();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("EditorPane.selectionBackground"));
		panel.setForeground(UIManager.getColor("Button.select"));
		panel.setBorder(new TitledBorder(new LineBorder(new Color(128, 128, 128), 2), "Forgot Passowrd", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 0)));
		panel.setBounds(0, 6, 344, 236);
		contentPane.add(panel);
		panel.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBounds(133, 33, 205, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(133, 75, 205, 20);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(133, 147, 205, 20);
		panel.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(133, 187, 205, 20);
		panel.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setBounds(5, 35, 116, 16);
		panel.add(lblUserName);
		
		JLabel lblSecurityQuestion = new JLabel("Security Question");
		lblSecurityQuestion.setBounds(5, 77, 116, 16);
		panel.add(lblSecurityQuestion);
		
		JLabel lblAnswer = new JLabel("Answer");
		lblAnswer.setBounds(6, 149, 71, 16);
		panel.add(lblAnswer);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(6, 189, 61, 16);
		panel.add(lblPassword);
		
		JButton btnNewButton = new JButton("back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				try {
					Login l=new Login();
					l.setVisible(true);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnNewButton.setBounds(345, 191, 99, 29);
		contentPane.add(btnNewButton);
		
		JButton btnRetrive = new JButton("Retrive");
		btnRetrive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				retrive();
			}
		});
		btnRetrive.setBounds(345, 150, 99, 29);
		contentPane.add(btnRetrive);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				search();
			}
		});
		btnSearch.setBounds(345, 29, 99, 29);
		contentPane.add(btnSearch);
	}
}
