package libraryManagement;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;

public class Login extends JFrame {
	Connection con;
	ResultSet rs;
	PreparedStatement pst;

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws ClassNotFoundException 
	 */
	public Login() throws ClassNotFoundException {
		super("login");
		con= DatabaseConection.connectionDB();
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(16, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.CYAN);
		panel.setBackground(Color.CYAN);
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Sign Up", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		panel.setBounds(16, 21, 364, 233);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("UserName");
		lblNewLabel_1.setBounds(17, 44, 80, 16);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("Password");
		lblNewLabel.setBounds(17, 81, 61, 16);
		panel.add(lblNewLabel);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(107, 76, 214, 26);
		panel.add(passwordField);
		
		JButton btnNewButton = new JButton("Log in");
		btnNewButton.setBounds(109, 119, 89, 29);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Sign Up");
		btnNewButton_1.setBounds(204, 119, 117, 29);
		panel.add(btnNewButton_1);
		
		JLabel lblNewLabel_2 = new JLabel("Trouble Logging In..");
		lblNewLabel_2.setBounds(18, 176, 130, 16);
		panel.add(lblNewLabel_2);
		lblNewLabel_2.setForeground(Color.RED);
		
		JButton btnNewButton_2 = new JButton("Forgot Passowrd..?");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				try {
					ForgotPassword fp=new ForgotPassword();
					fp.setVisible(true);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e1);
				}
				
				
			}
		});
		btnNewButton_2.setBounds(144, 171, 177, 29);
		panel.add(btnNewButton_2);
		
		textField = new JTextField();
		textField.setBounds(107, 39, 214, 26);
		panel.add(textField);
		textField.setColumns(10);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				try {
					SignUp sp=new SignUp();
					sp.setVisible(true);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e1);
				}
				
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sql="select * from account where username=? and password=? ";
				try {
					pst=con.prepareStatement(sql);
				pst.setString(1,textField.getText());
				pst.setString(2, passwordField.getText());
					rs=pst.executeQuery();	
					if(rs.next()) {
						rs.close();
						pst.close();
						setVisible(false);
						Loading ob=new Loading();
						ob.setVisible(true);
					}
					}
				 catch (SQLException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e1);

				}
				
				
			}
		});
	}
}
