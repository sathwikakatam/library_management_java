package libraryManagement;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.UIManager;
import javax.swing.JPasswordField;
import javax.swing.DefaultComboBoxModel;

public class SignUp extends JFrame {

Connection con;
ResultSet rs;
PreparedStatement pst;
 

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_4;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SignUp frame = new SignUp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	//DatabaseConection db=new DatabaseConection();
	/**
	 * Create the frame.
	 * @throws ClassNotFoundException 
	 */
	public SignUp() throws ClassNotFoundException {
		super("login");
		con= DatabaseConection.connectionDB();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("FormattedTextField.selectionBackground"));
		panel.setForeground(UIManager.getColor("FormattedTextField.selectionBackground"));
		panel.setBorder(new TitledBorder(new LineBorder(new Color(255, 102, 102)), "Sign Up", TitledBorder.LEADING, TitledBorder.TOP, null, UIManager.getColor("Button.select")));
		panel.setBounds(0, 19, 444, 253);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(58, 23, 111, 16);
		panel.add(lblNewLabel);
		
		JLabel lblUsername = new JLabel("UserName");
		lblUsername.setBounds(58, 64, 111, 16);
		panel.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(58, 107, 111, 16);
		panel.add(lblPassword);
		
		JLabel lblSecurityQuestion = new JLabel("Security Question");
		lblSecurityQuestion.setBounds(58, 148, 111, 16);
		panel.add(lblSecurityQuestion);
		
		JLabel lblAnswer = new JLabel("Answer");
		lblAnswer.setBounds(58, 184, 111, 16);
		panel.add(lblAnswer);
		
		textField = new JTextField();
		textField.setBounds(183, 18, 174, 26);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(183, 59, 174, 26);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(183, 179, 174, 26);
		panel.add(textField_4);
		textField_4.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"What is your First Name?", "What is your Mother tongue?", "Who is your childHood friend?", "What is your favorite place?"}));
		comboBox.setBounds(183, 144, 174, 27);
		panel.add(comboBox);
		
		JButton btnNewButton = new JButton("Create");
		btnNewButton.setBounds(133, 217, 117, 29);
		panel.add(btnNewButton);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Login ob;
				try {
					ob = new Login();
					ob.setVisible(true);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnBack.setBounds(249, 217, 117, 29);
		panel.add(btnBack);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(181, 102, 176, 26);
		panel.add(passwordField);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sql="insert into account(username,password,sec_question,answer) values(?,?,?,?)";
				try {
					pst=con.prepareStatement(sql);
					pst.setString(1,textField.getText());
					pst.setString(2, textField_1.getText());
					pst.setString(3,(String)comboBox.getSelectedItem());
					pst.setString(4,textField_4.getText());
					pst.execute();
					JOptionPane.showMessageDialog(null,"Your account has been created");
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null,e1);
				}
				
				
			}
		});
	}
}
